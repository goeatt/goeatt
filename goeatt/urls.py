
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings

from goeattapp import views, apis

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home, name='home'),
    url(r'^vendor/sign-in/$', auth_views.LoginView.as_view(template_name='vendor/sign-in.html'), {'template_name':'vendor/sign-in.html'}, name='vendor-sign-in'),
    url(r'^vendor/sign-out$', auth_views.LogoutView, {'next_page': '/'}, name='vendor-sign-out'),
    url(r'^vendor/sign-up$', views.vendor_sign_up, name='vendor-sign-up'),
    url(r'^vendor/$', views.vendor_home, name='vendor_home'),

    url(r'^vendor/account/$', views.vendor_account, name='vendor-account'),
    url(r'^vendor/meal/$', views.vendor_meal, name='vendor-meal'),
    url(r'^vendor/meal/add/$', views.vendor_add_meal, name='vendor-add-meal'),
    url(r'^vendor/meal/edit/(?P<meal_id>\d+)/$', views.vendor_edit_meal, name='vendor-edit-meal'),
    url(r'^vendor/order/$', views.vendor_order, name='vendor-order'),
    url(r'^vendor/report/$', views.vendor_report, name='vendor-report'),

    # Sign In/ Sign Up/ Sign Out
    url(r'^api/social/', include('rest_framework_social_oauth2.urls')),
    # /convert-token (sign in/ sign up)
    url(r'^api/vendor/order/notification/(?P<last_request_time>.+)/$', apis.vendor_order_notification),

    # APIs for CUSTOMERS
    url(r'^api/customer/vendors/$', apis.customer_get_vendors),
    url(r'^api/customer/meals/(?P<vendor_id>\d+)/$', apis.customer_get_meals),
    url(r'^api/customer/order/add/$', apis.customer_add_order),
    url(r'^api/customer/order/latest/$', apis.customer_get_latest_order),
    url(r'^api/customer/driver/location/$', apis.customer_driver_location),

    # APIs for DRIVERS
    url(r'^api/driver/orders/ready/$', apis.driver_get_ready_orders),
    url(r'^api/driver/order/pick/$', apis.driver_pick_order),
    url(r'^api/driver/order/latest/$', apis.driver_get_latest_order),
    url(r'^api/driver/order/complete/$', apis.driver_complete_order),
    url(r'^api/driver/revenue/$', apis.driver_get_revenue),
    url(r'^api/driver/location/update/$', apis.driver_update_location),



] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
