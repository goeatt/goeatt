# GoEatt - Food Delivery app



This my own application, which is similar to the renowned Uber Eats functionality. Only difference is that the business focuses on homechef's around Auckland, as compared to restuarants with Uber Eats.

Project Period: 2018-2020 

Application for the project is built using Python - Django framework using Rest API. 

We got 3 main user flow:
- Homechef managing her store with food items
- Customer, who orders the food from the preffered homechef store
- Delivery driver will be notified to be picked up for completing this order

We have made use of Rest API to create endpoints available for the IOS/andriod developers to make following use case work:
- Customer API - (Search Food Items, Place order, track order, etc.. )
- Driver API - (Check available orders, complete order, calculate review, etc..)



## Project Preview

This is a Python django project. The related documents is seen below:

- [Architecture](https://drive.google.com/file/d/1f8xuD4lqR06nrXx7DKHdqAYA_yzQlcW8/view?usp=sharing)
- [Database Schema](https://drive.google.com/file/d/1s2fytOA_zurqVubELIM499txtmTGg7jF/view?usp=sharing)
- [Driver Use Case](https://drive.google.com/file/d/1_V9eojQbxPA7xdXwAIkon-aPYHPFDChw/view?usp=sharing)
- [Customer Use Case](https://drive.google.com/file/d/1SncoaqnlsSdmO9rGZpRf9F25S9UQRXln/view?usp=sharing)

In a quick netshell, below is the 3 main functionality this application focuses on.

##### Homechef:
- Registration of the business
- Add food items willing to sell on our application - With Name, description, price and images
- Manage products from the dashboard
- When order received, pack the food and mark ready to be picked, so delivery drivers can be notified
- Reporting on sales of each homechef kitchen

##### Customers:
- Explore the food items from various homechefs on the shop. 
- Add preferred items to cart from one homechef only
- Complete order
- Tracking order delivery on real time

##### Delivery Driver:
- Using cron, driver API is looking for orders to be picked up
- Once available, accept the order
- Lcoation of the driver is pinged to update live location to be used by the customers to track the order
- Calculate revenue


## Requirements

1) Python Django Framework
2) RestAPI
3) Postgres DB
5) HTML/CSS/JS

## Installation

1) Create a folder '***workspace***' and open this folder in your editor (VS Code preffered). **Open the editor terminal**.

2) Go to your 'workpace' directory in the editor terminal and git pull the code using the command below:

***- % cd into the 'workspace' directory in the editor teminal before proceeding. You can copy n paste all the commands in this file.***

% cd workspace

```

git clone git@gitlab.com:goeatt/goeatt.git

```

3) Create virtualenv

```

python -m venv myenv

```

4) Enable virtualenv (*Please setup your own virtualenv before enabling*)

```

source myenv/bin/activate

```

5) Go inside the app main directory 'goeatt'.

```

cd goeatt/

```

6) Install the dependencies from requirements.txt

```

pip install -r requirements.txt

```

7) Optional, upgrade pip, if alered:

```

pip install --upgrade pip

```

8) Duplicate existing 'env.example' file and name it '.env'

```

cp env.example .env

```

9) Once .env file is created, provide the secure information

10) Update '.env' file with all the required variables assigned.

11) Run Django app
  

This project entire documenation for each of the below case will be provided on request.
1) Homechecf
2) Customer
3) Delivery Driver


## Author / Developer

Pavan Shashidharan
pavanshashidharan@gmail.com
+64 221668145