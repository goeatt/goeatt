from django.contrib import admin
from goeattapp.models import Vendor, Customer, Meal, Driver, Meal, Order, OrderDetails

admin.site.register(Vendor)
admin.site.register(Customer)
admin.site.register(Driver)
admin.site.register(Meal)
admin.site.register(Order)
admin.site.register(OrderDetails)
# Register your models here.
