Customer API's:


1) Get all the vendor listed [GET]
  - http://127.0.0.1:8000/api/customer/vendors/
  - Refer to get_vendor.png

2) Get all meals by "hotel id" [GET]
  - http://127.0.0.1:8000/api/customer/meals/1
  - Refer to get_all_meal_from_vendor.png

3) Add new order by the customer [POST]
  - http://localhost:8000/api/customer/order/add/
  - Refer Image: add_new_order.png

4) Customer check latest order
  - http://localhost:8000/api/customer/order/latest?access_token=<AACCESS-TOKEN>
  - Refer to customer_check_latest_order.png



Driver API's:

1) Get ready order [GET]
  - http://localhost:8000/api/driver/orders/ready

2) Driver pickup order [POST]
  - http://localhost:8000/api/driver/order/pick/?access_token&order_id
  - Refer to driver_pickup_order.png

3) Driver get latest order  [GET]
  - http://localhost:8000/api/driver/order/latest/?access_token=rHqa1An7Ii1xsIPX7UcKxOY2nK0l5o
  - Refer to driver-get-latest-order.png


4) Driver order complete [POST]
  - http://localhost:8000/api/driver/order/complete/
  - Refer to driver_order_complete.png

5) Driver get revenue (One Week) [GET]
  - http://localhost:8000/api/driver/revenue/?access_token=rHqa1An7Ii1xsIPX7UcKxOY2nK0l5o
  - Refer to driver_revenue.png
